import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../service/article.service';

@Component({
  selector: 'app-ajout-article',
  templateUrl: './ajout-article.page.html',
  styleUrls: ['./ajout-article.page.scss'],
})
export class AjoutArticlePage implements OnInit {
  valueChampTitre: string = "";
  valueChampTexte: string = "";

  constructor(
    public articleService: ArticleService,
    public router: Router,
  ) {}

  ngOnInit() {
  }

  ajoutArticle() {
    if(this.valueChampTitre != "" && this.valueChampTexte !="") {
    this.articleService.ajouterArticle(this.valueChampTitre, this.valueChampTexte);

    } else {
      console.error("Attention vous affichez un article incomplet!");
      
    }

    this.router.navigate(["home"]);
  }



}
