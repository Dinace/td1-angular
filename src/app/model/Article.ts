export class Article {

    title: string = "";
    content: string = "";

    constructor(_title: string, _content: string) {

        this.title = _title;
        this.content = _content;
    }

    
}

 