import { Article } from "../model/Article";
import { getStorage, setStorage } from "./storage";

export class ArticleService {
//création ou instanciation des articles
    article1 = new Article("titre1", "contenu1");
    article2 = new Article("titre2", "contenu2");
//Mettre dans le tableau les articles créés au-dessus
    listArticles: Article [] = [this.article1, this.article2];


    ajouterArticle(_titre: string, _content: string) {  //
        this.listArticles.push(new Article (_titre, _content));//On met ici des ojets articles dans le tableau  
        this.sauvegarde();
    }

    getArticle(index: number) {
        if(this.listArticles[index]!= null) {
            return this.listArticles[index];
        } else {
            return null;
        }
    }
    
    sauvegarde() {
        setStorage("listArticles", this.listArticles);
    }

    async loadListArticles() {
        const resultListArticles = await getStorage("listArticles");
        if(resultListArticles != null && Array.isArray(resultListArticles)) {
             this.listArticles = resultListArticles;   
        } else {
            this.listArticles = [];
        }

    }
    
    
}

   


